//Napisać program generujący wektor n liczb losowych całkowitych z przedziału <-100, 100>
//a) Program zwraca sumę wszystkich elementów wektora
//b) Średnią arytmetyczną i geometryczną wszystkich elementów wektora

function zadanie2
    
    //clear; clc;
    clear;
    
    printf("Pprogram generujący wektor n liczb losowych całkowitych z przedziału <-100, 100>\n");
    printf("a) Program zwraca sumę wszystkich elementów wektora\n");
    printf("b) Średnią arytmetyczną i geometryczną wszystkich elementów wektora\n\n");
    
    n = input("Podaj długość wektora: "); //przypisujemy wartość podaną przez użytkownika do n
    if n < 1  // zakładamy, że wektor musi posiadać jakieś wartości, czyli jego długość ma być większa od 0
        while n < 1 // będziemy próbować tak długo, aż użytkownik się podda i poda odpowiednią wartość. albo zamknie program.
            n = input("Podaj długość wektora większą od 0: ");
        end
        printf("Podana wartość wynosi %i.\nDziękuję za współpracę :)\n", n); // wypisujemy na ekran komunikat o wartości n i podziękowaniem, że użytkownik się dostosował
    else // użytkownik był posłuszny od początku, specjalne podziękowania nie są potrzebne.
        printf("Podana wartość wynosi %i.\n", n);
    end
    
    wektor = grand(1,n, "uin", -100,100); //losujemy nasz wektor - macierz o wielkości 1xn, liczb całkowitych, w przedizale <-100, 100>
    
    suma = 0;    // na początku suma elementów jest równa 0
    geomSr = 1;    // średnia geometryczna na początku wynosi 1, bo to jest element neutralny mnożenia.
    
    for i = 1 : n   // pętla, w której będizemy iterować (przechodzić po kolei po każdym) elementy wektora.
        suma = suma + wektor(i);  // zwiększamy naszą sumę o element wektora o indeksie równym i
        geomSr = geomSr * wektor(i); // i mnożymy przez siebie wszystkie elementy, będzie nam to potrzebne do obliczenia średniej geometrycznej
    end
    
    wektorString = string(wektor(1));   // tworzymy string zawierający wszystkie elementy naszego wektora
    
    // będziemy doklejać do wektorString przecinek i kolejny element wektora - pod warunkiem jednak, że długość wektora jest większa niż 1
    if n > 1 then
        for i = 2 : n
            wektorString = wektorString + ", " + string(wektor(i));
        end
    end
    
    printf("Wektor wygląda następująco: %s\n", wektorString);   // wypisujemy nasz wektor, a właściwie string zbudowany na jego podstawie
    
    printf("Suma elementów wynosi: %i\n", suma); // wypisujemy sumę elementów
    
    arytmSr = suma / n;  // obliczamy średnią arytmetyczną, bo mamy już wcześniej wyznaczoną sumę elementów (zmienna suma) i długość wektora (zmienna n)
    
    printf("Średnia arytmetyczna wynosi: %f\n", arytmSr);  // wypisujemy wynik na ekran
    
    geomSr = geomSr ^ (1/n); // obliczamy średnią geometryczną. zakładamy, że średnia geometryczna może być obliczona również ze zbioru elementów, w którym są liczby ujemne.
    
    if imag(geomSr) == 0 then
        printf("Średnia geometryczna wynosi: %f\n", geomSr);
    else
        if imag(geomSr) > 0 then
            printf("Średnia geometryczna wynosi: %g + %gi\n", real(geomSr), imag(geomSr));
        else
            printf("Średnia geometryczna wynosi: %g - %gi\n", real(geomSr), imag(geomSr));
        end
    end
    
endfunction
